.. Theory Docs documentation master file, created by
   sphinx-quickstart on Sun Nov 19 15:28:19 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. image:: /images/theory_logo_horizontal.png

Welcome to Theory Docs!
=======================


.. toctree::
   :maxdepth: 2
   :titlesonly:

   general_info/index
   workflows/index
   theory_tools/index


External Links
**************

**MakeTheory**
   https://maketheory.com/chaos/

**TheorySync Client**

      Windows
      Linux
      MacOSX

**Blender User Manual**
   https://docs.blender.org/manual/en/dev/index.html

**Blender Python API**
   https://docs.blender.org/api/current/

