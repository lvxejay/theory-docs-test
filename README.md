# Theory Studios Internal Documentation

This repository contains all of our internal documentation, built with Sphinx, and styled
with the Read the Docs theme. 

## Quickstart Guide:

* How to Contribute:

    1) Clone the repo
    
    2) Run `pip install -r requirements.txt` in your shell.
    
    3) Make any additions or changes to the documentation
    
    4) cd into `theory-docs/docs/`
    
    5) Run `make html` to build the docs locally
    
    6) Run `git add .`, 
        Then run `git commit -m "<insert commit message here>"`. 
        Finally run `git push -u origin` to push the changes up to the repo.
  
### ReStructuredText Primer:
ReStructuredText (.rst) is very similar to markdown.

All .html files are built with *Sphinx*.

You can find a primer on how to use it here: http://www.sphinx-doc.org/en/stable/rest.html
    
### Read the Docs (RTD):
While we won't be using ReadTheDocs directly, you can find additional information about
*Sphinx*, *ReStructuredText*, and related stuff here: http://docs.readthedocs.io/en/latest/getting_started.html